# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

--

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

--

**Kalimat ini dicetak tebal menggunakan 2 simbol bintang depan belakang** <br/>
*Kalimat ini dicetak miring menggunakan 1 simbol bintang depan belakang* <br/>
~~Kalimat ini dicetak coret menggunakan 2 simbol tilde~~ <br/>

__Kalimat ini dicetak tebal menggunakan 2 simbol underscore depan belakang__ <br/>
_Kalimat ini dicetak miring menggunakan 1 sombol underscore depan belakang_ <br/>

--

[google](https://google.com/) <br/>
[gitlab pribadi](https://gitlab.com/hudsam) <br/>

--

![logo google](https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png)
![logo web pribadi](http://hudsam.xyz/assets/img/favicon.png)

--

### Daftar unordered list menggunakan simbol asterisk
* Monitor
	* LCD
	* Tabung
* Mouse
	* Wireless
	* Wired
* Keyboard
	* Mekanik
	* Biasa
* CPU
	* Gaming
	* Office

### Daftar unordered list menggunakan simbol minus
- Monitor
	- LCD
	- Tabung
- Mouse
	- Wireless
	- Wired
- Keyboard
	- Mekanik
	- Biasa
- CPU
	- Gaming
	- Office

--

### Daftar ordered list menggunakan angka
1. Monitor
	1. LCD
	2. Tabung
2. Mouse
	1. Wireless
	2. Wired
3. Keyboard
	1. Mekanik
	2. Biasa
4. CPU
	1. Gaming
	2. Office

--

### Membuat daftar rencana dalam waktu dekat....
- [x] Belajar Git di GitLab
- [ ] Melengkapi form pendaftaran yudisium dan wisuda
- [ ] __Apa lagi ya?__

--

> Bermain adalah proses belajar secara sukarela (tanpa paksaan). -hudsam

--

Gunakan perintah `ls` untuk mengetahui isi setiap folder dan dokumennya bisa kita pelajari lebih lanjut dengan perintah `man ls`

--

```
$(document).ready(function(){
	$('.profil_siswa').on('click',function(){
		var Link = $(this).attr('href');
		$('.modal-content').load(Link,function(){
			$('#profil').modal({show:true});
		});
	});
});
```

--

| Nama Depan | Nama Tengah | Nama Bekalang |
| ---------- | ----------- | ------------- |
| Khusnul    |             | Huda          |
| X          |             |               |
| X          | Y           |               |
| X          | Y           | Z             |
|          X |          Y  |Z              |

--